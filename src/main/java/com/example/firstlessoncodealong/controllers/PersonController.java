package com.example.firstlessoncodealong.controllers;

import com.example.firstlessoncodealong.data_access.IPersonRepository;
import com.example.firstlessoncodealong.models.Person;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api")
public class PersonController {

    private final IPersonRepository personRepo;

    public PersonController(IPersonRepository personRepo) {
        this.personRepo = personRepo;
    }

    @PostMapping("person")
    public Person createPerson(@RequestBody Person person) {
        Person createdPerson = personRepo.save(person);
        return createdPerson;
    }

    @GetMapping("person/{id}")
    public Person getById(@PathVariable long id) {
        if (personRepo.existsById(id)) {
            return personRepo.findById(id).get();
        }
        return null;
    }

}

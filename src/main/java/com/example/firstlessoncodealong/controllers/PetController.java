package com.example.firstlessoncodealong.controllers;

import com.example.firstlessoncodealong.data_access.IPetRepository;
import com.example.firstlessoncodealong.models.Pet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api")
public class PetController {

    @Autowired
    private IPetRepository petRepo;

    @PostMapping("pet")
    public Pet createPet(@RequestBody Pet pet) {

        Pet createdPet = petRepo.save(pet);
        return createdPet;
    }

    @GetMapping("pet/{id}")
    public Pet getById(@PathVariable long id) {
        if (petRepo.existsById(id)) {
            return petRepo.findById(id).get();
        }
        return null;
    }
    @GetMapping("pet")
    public List<Pet> getAll(){
        return petRepo.findAll();
    }

}

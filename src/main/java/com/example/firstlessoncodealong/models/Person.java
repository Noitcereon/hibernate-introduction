package com.example.firstlessoncodealong.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column
    public String name;

    @Column
    public String contact;

    @Column
    public String email;


    @JsonGetter("pets")
    public List<String> get_pets_list() {
        return pets.stream()
                .map(pet -> {
                    return "/api/pet/" + pet.getId();
                }).collect(Collectors.toList());
    }


    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY)
    List<Pet> pets = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

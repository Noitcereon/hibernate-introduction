package com.example.firstlessoncodealong.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;

@Entity
public class Pet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column
    public String name;

    @Column
    public int age;

    @JsonGetter("owner")
    public String owner(){
        if(owner != null){
            return "/api/person/" + owner.getId();
        }
        return null;
    }

    @ManyToOne
    @JoinTable(
            name = "owner",
            joinColumns = {@JoinColumn(name = "pet_id")},
            inverseJoinColumns = {@JoinColumn(name = "owner_id")}
    )
    public Person owner;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

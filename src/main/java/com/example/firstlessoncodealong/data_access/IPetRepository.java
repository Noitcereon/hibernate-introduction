package com.example.firstlessoncodealong.data_access;

import com.example.firstlessoncodealong.models.Pet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPetRepository extends JpaRepository<Pet, Long> {

}

package com.example.firstlessoncodealong.data_access;

import com.example.firstlessoncodealong.models.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPersonRepository extends JpaRepository<Person, Long> {

}

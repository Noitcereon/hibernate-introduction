This is a simple project demonstrating default CRUD with Hibernate.

It was a code-along lesson.

Key take aways:

- Use JSON-Getter for linked data (if a Customer entity is references, map it as the path to that entity)
- Hibernate implements default repository with JpaRepository interface
- You generate the database schema based on the models you make (annotations)
